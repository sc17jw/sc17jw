// This file is part of coursework 2 for COMP1212
// which follows the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// Written by Sam Wilson

/**
 * Memory of 64 registers, each 16 bit-wide. Out hold the value
 * stored at the memory location specified by address. If load=1, then
 * the x value is loaded into the memory location specified by address
 * (the loaded value will be emitted to out after the next time step.)
 */

CHIP RAM64 {
    IN x[16], load, address[6];
    OUT out[16];

    PARTS:
    DMux8Way(in=load,sel=address[3..5],s=s, t=t, u=u, v=v, w=w, x=m, y=y, z=z);
    RAM8(x=x,load=s,address=address[0..2],out=a);
    RAM8(x=x,load=t,address=address[0..2],out=b);
    RAM8(x=x,load=u,address=address[0..2],out=c);
    RAM8(x=x,load=v,address=address[0..2],out=d);
    RAM8(x=x,load=w,address=address[0..2],out=e);
    RAM8(x=x,load=m,address=address[0..2],out=f);
    RAM8(x=x,load=y,address=address[0..2],out=g);
    RAM8(x=x,load=z,address=address[0..2],out=h);
    Mux8Way16(s=a,t=b,u=c,v=d,w=e,x=f,y=g,z=h,sel=address[3..5],out=out);
}
