// This file is part of coursework 2 for COMP1212
// which follows the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// Written by Sam Wilson

/**
 * Memory of 16K registers, each 16 bit-wide. Out holds the value
 * stored at the memory location specified by address. If load=1, then
 * the x value is loaded into the memory location specified by address
 * (the loaded value will be emitted to out after the next time step.)
 */

CHIP RAM16K {
    IN x[16], load, address[14];
    OUT out[16];

    PARTS:
    DMux8Way(in=load,sel=address[11..13],s=s, t=t, u=u, v=v, w=w, x=m, y=y, z=z);
    RAM4K(x=x,load=s,address=address[0..11],out=a);
    RAM4K(x=x,load=t,address=address[0..11],out=b);
    RAM4K(x=x,load=u,address=address[0..11],out=c);
    RAM4K(x=x,load=v,address=address[0..11],out=d);
    RAM4K(x=x,load=w,address=address[0..11],out=e);
    RAM4K(x=x,load=m,address=address[0..11],out=f);
    RAM4K(x=x,load=y,address=address[0..11],out=g);
    RAM4K(x=x,load=z,address=address[0..11],out=h);
    Mux8Way16(s=a,t=b,u=c,v=d,w=e,x=f,y=g,z=h,sel=address[11..13],out=out);
}
